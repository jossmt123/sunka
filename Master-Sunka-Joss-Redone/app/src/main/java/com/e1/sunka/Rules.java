package com.e1.sunka;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

public class Rules extends AppCompatActivity {

    private Context mcontext;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        setContentView(R.layout.activity_rules);
        loadRules();

    }

    public void loadRules(){
        TextView rules = (TextView)findViewById(R.id.textView4);
        rules.setText("In the first turn of the game, both players begin redistributing shells " +
                "according to the principles explained above simultaneously. " +
                "The first player to finish their move is the one who gets to move in the second turn. " +
                "After the first turn, players move alternately." + "\n" + "If the last shell that is redistributed" +
                " in a move drops in the player's own store, then the player gets an additional move. " +
                "Note that such bonus moves can be accumulated." + "\n" + "If the last shell that is redistributed" +
                " in a move drops in an empty small tray (not the store) on the player's own side," +
                " then that shell and all the shells in the corresponding tray on the opposite side " +
                "(i.e. the opponent's side) are captured and moved to the player's store." + "\n" + "A player must make " +
                "a move in his/her turn when one is possible. " +
                "If a player cannot make a move in his/her turn (because there are no shells in any of the small trays on his/her side), " +
                "that player must pass until a move is possible in a future turn.");

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_rules, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    public void goBack(View view){
        Intent intent = new Intent(this, startScreen.class);
        startActivity(intent);
        finish();
    }
}
