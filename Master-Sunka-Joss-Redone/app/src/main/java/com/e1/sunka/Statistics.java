package com.e1.sunka;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Scanner;

public class Statistics extends AppCompatActivity {

    ArrayList<Player> A1 = new ArrayList<Player>();
    ArrayList<Player> sortPlay = new ArrayList<Player>();

   /** Intent intent = getIntent();
    Bundle b = intent.getExtras();
    String p = (String) b.get("scoresC");

    public ArrayList<Player> makeArray(String p){
        Scanner scanner = new Scanner(p);
        while(scanner.hasNextLine())
        {
            String part = scanner.nextLine();
            int space1=part.indexOf(" ");
            int space2=part.lastIndexOf(" ");
            int score1 = Integer.parseInt(part.substring(0, space1));
            String name1 = part.substring(space1 + 1, space2);
            int won1 = Integer.parseInt(String.valueOf(part.charAt(space2+1)));
            Player PO = new Player(score1,name1,won1);
            System.out.println(PO.toString());
            A1.add(PO);
        }
        return A1;
    }
**/

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_statistics);
        setTitle("High Scores");
        Intent intent = getIntent();
        String[] names = intent.getStringArrayExtra("Playernames");
        int[] pscorewin = intent.getIntArrayExtra("Players");
        Log.d("Statistics.java", "names" + names[0] + " " + names[1]);
        Log.d("Statistics.java", "scores" + pscorewin[0] +" " + pscorewin[2]);
        Player player1 = new Player(pscorewin[0], names[0], pscorewin[1]);
        Player player2 = new Player(pscorewin[2], names[1], pscorewin[3]);
        A1.add(player1);
        A1.add(player2);

        statFile(A1);
        Log.d("Statistics.java", "printing array " + A1);

    }


    public void statFile(ArrayList<Player> A1) {
        String filename = "scores";
        FileOutputStream outputStream;

        try {
            outputStream = openFileOutput(filename, Context.MODE_APPEND);
//                ByteArrayOutputStream os = new ByteArrayOutputStream();
//                String aString = new String(os.toByteArray(),"UTF-8");
//                outputStream.write(aString.getBytes());
            for(int i=0; i<A1.size(); i++){
                outputStream.write(A1.get(i).toString().getBytes());
            }


            FileInputStream fis = openFileInput("scores");
            StringBuilder builder = new StringBuilder();
            int ch;
            while((ch = fis.read()) != -1){
                builder.append((char) ch);
            }

            String t = builder.toString();

//            int countspaces=0;
//            for(int i=0; i<t.length(); i++){
//                if(t.contains("\n")){
//                    countspaces++;
//                }
//            }
//           // for(int i=0; i<countspaces-1; i++)
            String p = t.substring(0,t.length()-1);
            Scanner scanner = new Scanner(p);
            System.out.println(p);
            while(scanner.hasNextLine())
            {
                String part = scanner.nextLine();
                int space1=part.indexOf(" ");
                int space2=part.lastIndexOf(" ");
                int score1 = Integer.parseInt(part.substring(0, space1));
                String name1 = part.substring(space1 + 1, space2);
                int won1 = Integer.parseInt(String.valueOf(part.charAt(space2+1)));
                Player PO = new Player(score1,name1,won1);
                System.out.println(PO.toString());
                sortPlay.add(PO);
            }
            Collections.sort(sortPlay, new Comparator<Player>() {
                @Override
                public int compare(Player p1, Player p2) {
                    return p2.score - p1.score; // Descending
                }
            });

            String finalPlayersNames="";
            String finalPlayerScores="";
            String finalPlayerWon="";
            for(int i=0; i<sortPlay.size(); i++){
                Player J = sortPlay.get(i);
                finalPlayersNames = finalPlayersNames+J.name+"\n";
                finalPlayerScores = finalPlayerScores+ J.score +"\n";
                finalPlayerWon = finalPlayerWon+J.win+"\n";

            }

            TextView T1 = (TextView)(findViewById(R.id.playerNames));
            T1.setText(finalPlayersNames);
            TextView T2 = (TextView)(findViewById(R.id.playerScores));
            T2.setText(finalPlayerScores);
            TextView T3 = (TextView)(findViewById(R.id.playerGameWon));
            T3.setText(finalPlayerWon);
            outputStream.close();
        }catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void startScreen(View view){
        Intent intent = new Intent(this, startScreen.class);
        startActivity(intent);
    }

}

