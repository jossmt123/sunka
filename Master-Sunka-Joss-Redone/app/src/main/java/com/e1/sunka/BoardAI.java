package com.e1.sunka;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.Arrays;
import java.util.Random;

public class BoardAI extends AppCompatActivity {


    Button buttons[] = new Button[16];
    TextView[] buttonValues = new TextView[14];
    public int currentIndex;
    Move move = new Move();
    public int value; Random rand = new Random();
    public String set1; public String set2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_board_ai);
        move.newGame();

        TextView P2name = (TextView) (findViewById(R.id.p2Pot));
        Intent intent = getIntent();
        Bundle b = intent.getExtras();
        set2= (String) b.get("NAME2");
        P2name.setText(set2);
        set1 = (String) b.get("NAME1");
        TextView P1name = (TextView) (findViewById(R.id.p1Pot));
        P1name.setText(set1);

        //P2pot the followed by all of p1 holes and text views
        buttons[0] = (Button) findViewById(R.id.p2Pot);
        buttons[1] = (Button) findViewById(R.id.p1Hole1);buttonValues[0] = (TextView)findViewById(R.id.p1Hole1Text);
        buttons[2] = (Button) findViewById(R.id.p1Hole2);buttonValues[1] = (TextView)findViewById(R.id.p1Hole2Text);
        buttons[3] = (Button) findViewById(R.id.p1Hole3);buttonValues[2] = (TextView)findViewById(R.id.p1Hole3Text);
        buttons[4] = (Button) findViewById(R.id.p1Hole4);buttonValues[3] = (TextView)findViewById(R.id.p1Hole4Text);
        buttons[5] = (Button) findViewById(R.id.p1Hole5);buttonValues[4] = (TextView)findViewById(R.id.p1Hole5Text);
        buttons[6] = (Button) findViewById(R.id.p1Hole6);buttonValues[5] = (TextView)findViewById(R.id.p1Hole6Text);
        buttons[7] = (Button) findViewById(R.id.p1Hole7);buttonValues[6] = (TextView)findViewById(R.id.p1Hole7Text);
        //p1 pot then followed by all of p2 holes and text view
        buttons[8] = (Button) findViewById(R.id.p1Pot);
        buttons[9] = (Button) findViewById(R.id.p2Hole1);buttonValues[7] = (TextView)findViewById(R.id.p2Hole1Text);
        buttons[10] = (Button) findViewById(R.id.p2Hole2);buttonValues[8] = (TextView)findViewById(R.id.p2Hole2Text);
        buttons[11] = (Button) findViewById(R.id.p2Hole3);buttonValues[9] = (TextView)findViewById(R.id.p2Hole3Text);
        buttons[12] = (Button) findViewById(R.id.p2Hole4);buttonValues[10] = (TextView)findViewById(R.id.p2Hole4Text);
        buttons[13] = (Button) findViewById(R.id.p2Hole5);buttonValues[11] = (TextView)findViewById(R.id.p2Hole5Text);
        buttons[14] = (Button) findViewById(R.id.p2Hole6);buttonValues[12] = (TextView)findViewById(R.id.p2Hole6Text);
        buttons[15] = (Button) findViewById(R.id.p2Hole7);buttonValues[13] = (TextView)findViewById(R.id.p2Hole7Text);

        for (int i = 1; i < 16; i++) {
            final int currentI = i;
            if(currentI==8)continue;
            buttons[i].setOnClickListener(new Button.OnClickListener() {
                public void onClick(View v) {
                    // issue is with i value here
                    makeMove(move.cups.get(currentI));
                    updateButtons();

                }
            });
        }
        setTurnPlayer(1);
    }
    @Override
    public boolean onCreateOptionsMenu (Menu menu){
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_sunka_board, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected (MenuItem item){
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    public void alwaysprefferedmoves() {
        for (int i = 7; i >= 1; i--) {
            for (int j = 1; j <= 7; j++) {
                if (move.cups.get(j).pebbleCount == i) {
                    makeMove(move.cups.get(j));
                    break;
                }
            }
        }

        alwaysprefferedmoves();
    }

    public void leastprefferdmoves() {
        int CupA[] = new int[8];

        for (int i = 7; i <= 1; i--) {
            CupA[i]=move.cups.get(i).pebbleCount;
        }
        Arrays.sort(CupA);
        for (int j = 7; j <= 1; j--) {
            if(move.cups.get(j).pebbleCount==CupA[6]){
                makeMove(move.cups.get(j));
                break;
            }
        }


    }
    public void run(){
        Handler handler = new Handler();
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                updateButtons();
            }
        };
        runOnUiThread(runnable);
        runnable.run();
        handler.postDelayed(runnable, 5000);
    }

    public void makeMove(Cup cup) {
        value = cup.returnPebbleCount();
        currentIndex = move.cups.indexOf(cup);
        int lastIndex = currentIndex + value -1;
        cup.emptyCup();
        //endGame();
        for (int i = currentIndex; i < lastIndex + 1; i++) {
            //run();
            Cup nextCup = move.cups.get((i + 1) % 16);
            if (i == lastIndex) {
                setPlayer(lastIndex);
                checkEligible();
            } else if (nextCup.isP1Pot() && move.returnCurrentPlayer() == 2) {
                lastIndex++;
                continue;
            } else if (nextCup.isP2Pot() && move.returnCurrentPlayer() == 1) {
                lastIndex++;
                continue;
            } else {
                nextCup.increasePebbleCount();
            }
        }
    }

    public void setRandomPlayer(){
        boolean b = rand.nextBoolean();
        if(b){
            move.turn = 0;
        }else{
            move.turn = 1;
        }
        setTurnPlayer(move.turn);
    }


    public void setTurnPlayer(int n){

        if(n%2 != 0) {
            for (int i = 1; i < 8; i++) {
                buttons[i].setEnabled(true);
            }
            for (int i = 9; i < 16; i++) {
                buttons[i].setEnabled(false);
            }
        }else if(n%2 ==0){
            for(int i=1; i<8; i++){
                buttons[i].setEnabled(false);
            }
            for(int i=9; i<16; i++){
                buttons[i].setEnabled(false);
            }
        }
    }

    public void setNextPlayer(){
        move.turn++;
        setTurnPlayer(move.turn);
        runAi();
    }

    public void runAi(){
        if(move.returnCurrentPlayer()==2){
            alwaysprefferedmoves();
            leastprefferdmoves();
        }
    }


    public void setPlayer(int n) {
        Cup nextCup = move.cups.get((n+1)%16);
        if (nextCup.isPot == false && nextCup.pebbleCount == 0) {
            nextCup.increasePebbleCount();
            move.emptyCupSwitch(nextCup);
            setNextPlayer();
        } else if (nextCup.isP1Pot() && move.returnCurrentPlayer() == 1) {
            nextCup.increasePebbleCount();
        } else if (nextCup.isP2Pot() && move.returnCurrentPlayer() == 2) {
            nextCup.increasePebbleCount();
        } else {
            nextCup.increasePebbleCount();
            setNextPlayer();
        }
        updateButtons();
    }

    public void checkEligible(){
        if(move.p1ScoreCup.pebbleCount + move.p2ScoreCup.pebbleCount == 98){
            gameOver();
        }else if(move.checkPlayer1Cups()==0){
            setTurnPlayer(2);
        }else if (move.checkPlayer2Cups()==0){
            setTurnPlayer(1);
        }
    }


    public void gameOver(){

        int p1win;
        int p2win;

        if(move.p1ScoreCup.pebbleCount > move.p2ScoreCup.pebbleCount) {
            p1win = 1;
            p2win = 0;
        }else if(move.p1ScoreCup.pebbleCount == move.p2ScoreCup.pebbleCount){
            p1win = 2;
            p2win = 2;
        }
        else{
            p1win = 0;
            p2win = 1;
        }

        int[] pscorewin = {move.p1ScoreCup.pebbleCount, p1win, move.p2ScoreCup.pebbleCount, p2win};
        String[] pnames = {set1, set2};

        Intent intent = new Intent(BoardAI.this, Scores.class);
        intent.putExtra("Players", pscorewin);
        intent.putExtra("PlayerNames", pnames);
        startActivity(intent);
        finish();
    }

    public void endGame(){
        move.cups.get(0).pebbleCount = 48;
        move.cups.get(8).pebbleCount = 49;
    }


    public void updateButtons(){

        buttons[0].setText(move.cups.get(0).toString());
        buttons[8].setText(move.cups.get(8).toString());

        for(int i=0; i<14; i++){
            if(i>6){
                buttonValues[i].setText(move.cups.get(i+2).toString());
            }
            else{
                buttonValues[i].setText(move.cups.get(i+1).toString());
            }
        }

        for(int i=1; i<buttons.length; i++){
            if(i==8){
                continue;
            }else{

                int cupCount = move.cups.get(i).pebbleCount;

                switch(cupCount){
                    case 0: buttons[i].setForeground(getDrawable(R.drawable.pebbles1large));
                        break;
                    case 1:buttons[i].setForeground(getDrawable(R.drawable.pebbles1large));
                        break;
                    case 2: buttons[i].setForeground(getDrawable(R.drawable.pebbles2large));
                        break;
                    case 3: buttons[i].setForeground(getDrawable(R.drawable.pebbles3large));
                        break;
                    case 4: buttons[i].setForeground(getDrawable(R.drawable.pebbles4large));
                        break;
                    case 5: buttons[i].setForeground(getDrawable(R.drawable.pebbles5large));
                        break;
                    case 6: buttons[i].setForeground(getDrawable(R.drawable.pebbles6large));
                        break;
                    case 7: buttons[i].setForeground(getDrawable(R.drawable.pebbles7large));
                        break;
                    case 8: buttons[i].setForeground(getDrawable(R.drawable.pebbles8large));
                        break;
                    default: buttons[i].setForeground(getDrawable(R.drawable.pebbles9large));
                        break;
                }
            }
        }

    }

}
