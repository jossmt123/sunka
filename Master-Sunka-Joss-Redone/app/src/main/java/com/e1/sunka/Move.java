package com.e1.sunka;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Joss on 18/10/2015.
 */
public class Move{

    public ArrayList<Cup> cups;
    public int turn = 0;
    Cup p1ScoreCup;
    Cup p2ScoreCup;
    public void newGame() {

        cups = new ArrayList<>(16);

        p1ScoreCup = new Cup(0, 1, true);
        p2ScoreCup = new Cup(0, 2, true);
        cups.add(0, p2ScoreCup);

        //Reset all cups
        for (int i = 1; i < 8; i++) {
            Cup cup = new Cup(7, 1);
            cups.add(i, cup);
        }
        cups.add(8, p1ScoreCup);

        for (int i=9; i<16; i++){
            Cup cup = new Cup(7, 2);
            cups.add(i, cup);
        }

    }

    public void emptyCupSwitch(Cup cup) {
        int index = cups.indexOf(cup);
        Cup currentCup = cup;
        Cup oppCup;
        int currentCupValue = cup.pebbleCount;
        if(returnCurrentPlayer()==1){
        switch(index) {
            case 1:
                oppCup = cups.get(15);
                p1ScoreCup.pebbleCount += currentCupValue + oppCup.pebbleCount;
                oppCup.emptyCup();
                currentCup.emptyCup();
                break;
            case 2:
                oppCup = cups.get(14);
                p1ScoreCup.pebbleCount += currentCupValue + oppCup.pebbleCount;
                oppCup.emptyCup();
                currentCup.emptyCup();
                break;
            case 3:
                oppCup = cups.get(13);
                p1ScoreCup.pebbleCount += currentCupValue + oppCup.pebbleCount;
                oppCup.emptyCup();
                currentCup.emptyCup();
                break;
            case 4:
                oppCup = cups.get(12);
                p1ScoreCup.pebbleCount += currentCupValue + oppCup.pebbleCount;
                oppCup.emptyCup();
                currentCup.emptyCup();
                break;
            case 5:
                oppCup = cups.get(11);
                p1ScoreCup.pebbleCount += currentCupValue + oppCup.pebbleCount;
                oppCup.emptyCup();
                currentCup.emptyCup();
                break;
            case 6:
                oppCup = cups.get(10);
                p1ScoreCup.pebbleCount += currentCupValue + oppCup.pebbleCount;
                oppCup.emptyCup();
                currentCup.emptyCup();
                break;
            case 7:
                oppCup = cups.get(9);
                p1ScoreCup.pebbleCount += currentCupValue + oppCup.pebbleCount;
                oppCup.emptyCup();
                currentCup.emptyCup();
                break;

        }}
        else if(returnCurrentPlayer()==2) {
            switch (index) {
                case 9:
                    oppCup = cups.get(7);
                    p2ScoreCup.pebbleCount += currentCupValue + oppCup.pebbleCount;
                    oppCup.emptyCup();
                    currentCup.emptyCup();
                    break;
                case 10:
                    oppCup = cups.get(6);
                    p2ScoreCup.pebbleCount += currentCupValue + oppCup.pebbleCount;
                    oppCup.emptyCup();
                    currentCup.emptyCup();
                    break;
                case 11:
                    oppCup = cups.get(5);
                    p2ScoreCup.pebbleCount += currentCupValue + oppCup.pebbleCount;
                    oppCup.emptyCup();
                    currentCup.emptyCup();
                    break;
                case 12:
                    oppCup = cups.get(4);
                    p2ScoreCup.pebbleCount += currentCupValue + oppCup.pebbleCount;
                    oppCup.emptyCup();
                    currentCup.emptyCup();
                    break;
                case 13:
                    oppCup = cups.get(3);
                    p2ScoreCup.pebbleCount += currentCupValue + oppCup.pebbleCount;
                    oppCup.emptyCup();
                    currentCup.emptyCup();
                    break;
                case 14:
                    oppCup = cups.get(2);
                    p2ScoreCup.pebbleCount += currentCupValue + oppCup.pebbleCount;
                    oppCup.emptyCup();
                    currentCup.emptyCup();
                    break;
                case 15:
                    oppCup = cups.get(1);
                    p2ScoreCup.pebbleCount += currentCupValue + oppCup.pebbleCount;
                    oppCup.emptyCup();
                    currentCup.emptyCup();
                    break;
            }
        }
    }

    public int checkPlayer1Cups(){
        int cupP1Accum = 0;
        for(int i=1; i<8; i++){
            cupP1Accum += cups.get(i).pebbleCount;
        }
        Log.d("Move.java", "Print p1 cup accumulation:" + cupP1Accum);
        return cupP1Accum;
    }

    public int checkPlayer2Cups() {
        int cupP2Accum = 0;
        for (int i = 9; i < 16; i++) {
            cupP2Accum += cups.get(i).pebbleCount;
        }
        Log.d("Move.java", "Print p2 cup accumulation:" + cupP2Accum);
        return cupP2Accum;
    }

    public int returnCurrentPlayer(){
        if(turn%2==0){
            return 2;
        }else{
            return 1;
        }
    }



}




