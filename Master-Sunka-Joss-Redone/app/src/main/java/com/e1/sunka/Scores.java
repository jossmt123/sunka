package com.e1.sunka;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

public class Scores extends AppCompatActivity {

    public int[] pscorewin;
    public String[] pnames;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scores);

        Intent intent = getIntent();
        pscorewin = intent.getIntArrayExtra("Players");
        pnames = intent.getStringArrayExtra("PlayerNames");

        TextView player1name = (TextView) (findViewById(R.id.textView2));
        player1name.setText(pnames[0]);
        TextView player2name = (TextView) (findViewById(R.id.textView3));
        player2name.setText(pnames[1]);
        TextView player1score = (TextView) (findViewById(R.id.p1Score));
        player1score.setText("Scores:" + pscorewin[0]);
        TextView player2score = (TextView) (findViewById(R.id.p2Score));
        player2score.setText("Scores:" + pscorewin[2]);

        TextView playerWin = (TextView) (findViewById(R.id.playerWon));
        if(pscorewin[1] > pscorewin[3]){
            playerWin.setText("Winner: " + pnames[0]);
            playerWin.setTextColor(Color.BLUE);
        }else if(pscorewin[1] == pscorewin[3]){
            playerWin.setText("Draw");
        }else{
            playerWin.setText("Winner: " + pnames[1]);
            playerWin.setTextColor(Color.RED);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_scores, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void openScores(View view){
        Intent intent = new Intent(this, Statistics.class);
        Player P1 = new Player(pscorewin[0],pnames[0], pscorewin[1]);
        Player P2 = new Player(pscorewin[1],pnames[1], pscorewin[3]);
        String p = P1.toString()+P2.toString();
        //intent.putExtra("scoresC",p);
        intent.putExtra("Players", pscorewin);
        intent.putExtra("Playernames", pnames);
        startActivity(intent);
        finish();
    }

}
