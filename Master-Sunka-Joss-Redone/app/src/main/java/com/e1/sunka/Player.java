package com.e1.sunka;

import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Joss on 18/10/2015.
 */
public class Player extends SunkaBoard{

    public int score;
    public int win;
    public String name;

    public Player(int score, String name){
        this.score = score;
        this.name = name;
    }

    public Player(int score, String name, int win){
        this.score = score;
        this.name = name;
        this.win = win;
    }

    public String toString(){
        return score + " " + name + " " + win + "\n";
    }

}
