package com.e1.sunka;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toolbar;

public class NamesActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_names);

    }

    public void play(View view){

        EditText P2editname = (EditText) (findViewById(R.id.playername2));
        String name2 = P2editname.getText().toString();
        EditText P1editname = (EditText) (findViewById(R.id.playername1));
        String name1 = P1editname.getText().toString();
        Intent intent = new Intent(this, SunkaBoard.class);
        intent.putExtra("NAME2",name2);
        intent.putExtra("NAME1", name1);
        startActivity(intent);
    }

}