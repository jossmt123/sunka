package com.e1.sunka;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class AINames extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ainames);
    }
    public void playSingle(View view){

        EditText P2editname = (EditText) (findViewById(R.id.singlePlayername));
        String name2 ="Computer";
        String name1 = P2editname.getText().toString();
        Intent intent = new Intent(this, BoardAI.class);
        intent.putExtra("NAME2",name2);
        intent.putExtra("NAME1", name1);
        startActivity(intent);
        finish();
    }
}
