package com.e1.sunka;

import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Joss on 18/10/2015.
 */
public class Cup {

    public Cup(int pbc, int p){
        pebbleCount = pbc;
        player = p;
    }
    public Cup(int pbc, int p, boolean b){
        pebbleCount = pbc;
        player = p;
        isPot = b;
    }

    public int pebbleCount;
    public int player;
    public boolean isPot;

    public String toString(){
        return "" + pebbleCount;
    }

    public int returnPebbleCount(){
        return pebbleCount;
    }
    public void increasePebbleCount(){
        pebbleCount++;
    }

    public boolean isP1Pot(){
        if(isPot && player==1){
            return true;
        }else {
            return false;
        }
    }
    public boolean isP2Pot(){
        if(isPot && player==2){
            return true;
        }else {
            return false;
        }
    }
    public void emptyCup(){
        pebbleCount=0;
    }

    public boolean hasPebbles(){
        if(pebbleCount >0){
            return true;
        }else{
            return false;
        }
    }

}
